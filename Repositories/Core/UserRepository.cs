using api.Models;
using api.Models.Core;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

namespace api.Repositories.Core {
    public class UserRepository : Repository<User, UserGet, UserInsert, UserUpdate> {
        /* querys especificas de usuário */
        public UserRepository(GefinContext context) : base(context) {}
        public async Task<bool> IsEmailAvailable(string email) 
        {
            return await this.table.Where(u => u.Email == email).SingleOrDefaultAsync() == null;
        }
        public async Task<bool> IsLoginAvailable(string login) 
        {
            return await this.table.Where(u => u.Login == login).SingleOrDefaultAsync() == null;
        }
    }
}