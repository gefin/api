using Microsoft.EntityFrameworkCore;
using api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;
using AutoMapper;

namespace api.Repositories
{
    public abstract class Repository<TModel, TGet, TInsert, TUpdate> where TModel : Model
    {
        protected readonly DbSet<TModel> table;
        protected readonly GefinContext context;
        public Repository(GefinContext context)
        {
            
            this.context = context;
            this.table = context.Set<TModel>();
        }

        public IQueryable<TModel> Query()
        {
            return table;
        }

        public virtual async Task<TGet> GetSingleAsync(int id)
        {
            var entity = await table.Where(e => e.Id == id).SingleOrDefaultAsync();
            return Mapper.Map<TGet>(entity);
        }

        public virtual async Task<List<TGet>> GetAllAsync(Expression<Func<TModel, bool>> exp = null, int page = 1, int pageSize = int.MaxValue) 
        {
            var q = table.Skip((page - 1) * pageSize).Take(pageSize);
            if(exp != null) 
            {
                q = q.Where(exp);
            }
            return Mapper.Map<List<TGet>>(await q.ToListAsync());
        }

        public virtual async Task<TGet> AddAsync(TInsert entity) 
        {
            var newEntity = await context.AddAsync(Mapper.Map<TModel>(entity));
            await context.SaveChangesAsync();
            return Mapper.Map<TGet>(newEntity.Entity);
        }

        public virtual async Task<TModel> RemoveAsync(int id)
        {
            var entity = await table.Where(e => e.Id == id).SingleOrDefaultAsync();
            if(entity == null)
                return default(TModel);
            context.Remove(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public virtual async Task<TGet> UpdateAsync(TUpdate data, int id)
        {
            var entity = await table.Where(e => e.Id == id).SingleOrDefaultAsync();
            if(entity == null)
                return default(TGet);
            entity = Mapper.Map<TUpdate, TModel>(data, entity);
            await context.SaveChangesAsync();
            return Mapper.Map<TGet>(entity);
        }
    }
}
