using api.Models;
using api.Models.Finance;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace api.Repositories.Finance {
    public class IncomeRepository : Repository<Income, IncomeGet, IncomeInsert, IncomeInsert> {
        /* querys especificas de usuário */
        public IncomeRepository(GefinContext context) : base(context) {}
    }
}