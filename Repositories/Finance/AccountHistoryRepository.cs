using api.Models;
using api.Models.Finance;
using Microsoft.EntityFrameworkCore;

namespace api.Repositories.Finance {
    public class AccountHistoryRepository : Repository<AccountHistory, AccountHistory, AccountHistory, AccountHistory> {
        /* querys especificas de usuário */
        public AccountHistoryRepository(GefinContext context) : base(context) {}
    }
}