using api.Models;
using api.Models.Finance;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Repositories.Finance {
    public class IncomeHistoryRepository : Repository<IncomeHistory, IncomeHistory, IncomeHistory, IncomeHistory> {
        /* querys especificas de usuário */
        public IncomeHistoryRepository(GefinContext context) : base(context) {}
        public async Task<List<IncomeHistory>> ForIncome(int id, int page = 1, int pageSize = int.MaxValue)
        {
            return await table.Where(e => e.Income.Id == id).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        public async override Task<IncomeHistory> AddAsync(IncomeHistory history)
        {
            context.Set<Account>().ElementAt(1).Balance += history.Amount;
            await context.SaveChangesAsync();
            return history;
        }
        public override async Task<IncomeHistory> RemoveAsync(int id)
        {
            var entity = await base.RemoveAsync(id);
            if(entity == null)
                return null;
            context.Set<Account>().ElementAt(1).Balance -= entity.Amount;
            await context.SaveChangesAsync();
            return entity;
        }
    }
}