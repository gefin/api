using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Models.Finance;
using api.Models;
using Microsoft.EntityFrameworkCore;
using api.Repositories.Finance;

namespace api.Controllers.Core
{
    [Route("api/[controller]")]
    public class IncomeController : Controller
    {
        private IncomeRepository incomeRepository;
        private IncomeHistoryRepository incomeHistoryRepository;
        private GefinContext context;

        public IncomeController(GefinContext context)
        {
            this.context = context;
            this.incomeRepository = new IncomeRepository(context);
            this.incomeHistoryRepository = new IncomeHistoryRepository(context);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(int? page = 1, int? pageSize = int.MaxValue)
        {
            var incomeList = await incomeRepository.GetAllAsync();
            return Ok(incomeList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var income = await incomeRepository.GetSingleAsync(id);
            if (income == null)
                return NotFound();
            return Ok(income);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]IncomeInsert income)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var inc = await incomeRepository.AddAsync(income);
            return Ok(inc);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]IncomeInsert income)
        {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var entity = await incomeRepository.UpdateAsync(income, id);
            if (entity == null)
                return NotFound();
            return Ok(entity);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await incomeRepository.RemoveAsync(id);
            if (result == null)
                return NotFound();
            return Ok();
        }

        [HttpGet("{id}/history")]
        public async Task<IActionResult> GetHistory(int id, int page = 1, int pageSize = int.MaxValue)
        {
            return Ok(await incomeHistoryRepository.ForIncome(id, page, pageSize));
        }

        [HttpPost("api/incomehistory")]
        public async Task<IActionResult> PostHistory(int idIncome, [FromBody]IncomeHistory history)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                var entity = await incomeHistoryRepository.AddAsync(history);
                transaction.Commit();
                return Ok(entity);
            }
        }

        [HttpDelete("api/incomehistory/{idHist}")]
        public async Task<IActionResult> DeleteHistory(int idIncome, int idHist)
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                var entity = await incomeHistoryRepository.RemoveAsync(idHist);
                if(entity == null)
                {
                    return NotFound();
                }
                transaction.Commit();
                return Ok();
            }
        }
    }
}
