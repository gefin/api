using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Models.Core;
using api.Models;
using Microsoft.EntityFrameworkCore;
using api.Repositories.Core;

namespace api.Controllers.Core
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private UserRepository userRepository;
        private GefinContext context;

        public UsersController(GefinContext context) 
        {
            this.context = context;
            this.userRepository = new UserRepository(context);
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var userList = await userRepository.GetAllAsync();
            return Ok(userList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var usr = await userRepository.GetSingleAsync(id);
            if(usr == null)
                return NotFound();
            return Ok(usr);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]UserInsert usr)
        {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var newUsr = await userRepository.AddAsync(usr);
            return Ok(newUsr);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]UserUpdate user)
        {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var usr = await userRepository.UpdateAsync(user, id);
            if(usr != null) {
                return Ok();
            }
            return NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var usr = await userRepository.RemoveAsync(id);
            if(usr != null) {
                return Ok();
            }
            return NotFound();
        }

        [HttpGet("emailavailable")]
        public async Task<IActionResult> EmailAvailable(string email)
        {
            return Ok(await userRepository.IsEmailAvailable(email));  
        }

        [HttpGet("loginavailable")]
        public async Task<IActionResult> LoginAvailable([FromQuery]string login)
        {
            return Ok(await userRepository.IsLoginAvailable(login));
        }
    }
}
