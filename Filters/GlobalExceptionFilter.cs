using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using api.Models;

public class GlobalExceptionFilter : ExceptionFilterAttribute
{
    private GefinContext context;
    public GlobalExceptionFilter(GefinContext context) {
        this.context = context;
    }
    public override void OnException(ExceptionContext context)
    {
        this.context.Database.CurrentTransaction?.Rollback();
        context.Result = new ContentResult {
            Content = context.Exception.InnerException?.Message ?? context.Exception.Message,
            StatusCode = 500
        };
    }
}