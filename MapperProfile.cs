using AutoMapper;
using api.Models.Core;
using api.Models.Finance;

class MapperProfile : Profile
{
    public MapperProfile()
    {
        CreateMap<UserInsert, User>();
        CreateMap<User, UserGet>();
        CreateMap<UserUpdate, User>();

        CreateMap<IncomeInsert, Income>();
        CreateMap<Income, IncomeGet>();
    }
}