using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using api.Models.Core;
using api.Models.Finance;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace api.Models {
    public class GefinContext : DbContext {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            ILoggerFactory factory = new LoggerFactory().AddConsole().AddDebug();
            optionsBuilder.UseLoggerFactory(factory);
            optionsBuilder.UseSqlite(@"Data Source=gefin.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userBuilder = modelBuilder.Entity<User>();
            userBuilder.HasIndex(u => u.Login).IsUnique();
            userBuilder.HasIndex(u => u.Email).IsUnique();
            modelBuilder.Entity<Account>();
            modelBuilder.Entity<AccountHistory>().HasOne(e => e.Account).WithMany(e => e.AccountHistory);
            modelBuilder.Entity<Income>();
            modelBuilder.Entity<IncomeHistory>().HasOne(e => e.Income).WithMany(e => e.IncomeHistory);
        }
    }
}