using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.Models.Core
{
    public class UserInsert
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [MinLength(3)]
        public string Login { get; set; }
        [Required]
        [MinLength(3)]
        public string Password { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
