using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.Models.Core
{
    // TODO: Propriedades como senha e email terão processo específico para mudança
    public class UserUpdate
    {
        [Required]
        public string Name { get; set; }
    }
}
