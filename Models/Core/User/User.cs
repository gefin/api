using System;
using api.Models;

namespace api.Models.Core
{
    public class User : Model
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
