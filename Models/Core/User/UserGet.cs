using System;
using api.Models;

namespace api.Models.Core
{
    public class UserGet
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
