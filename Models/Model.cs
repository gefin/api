﻿using System;
using System.Collections.Generic;

namespace api.Models
{
    public abstract class Model
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }
}
