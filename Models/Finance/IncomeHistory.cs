using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.Models.Finance
{
    public class IncomeHistory : Model
    {
        [Required]
        public int Amount { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public string Obs { get; set; }
        public Income Income { get; set; }
        [Key]
        public int IncomeId { get; set; }
    }
}