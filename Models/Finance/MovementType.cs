using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.Models.Finance
{
    public enum MovementType
    {
        MonthlyFill = 0,
        MonthlyEmpty = 1,
        Purchase = 2,
        ManualTransfer = 3,
        InvestmentIncome = 4,
        TaxPay = 5
    }
}