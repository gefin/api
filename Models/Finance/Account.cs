using System;
using api.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace api.Models.Finance
{
    public class Account : Model
    {
        [Required]
        public string Name { get; set; }
        public int Balance { get; set; }
        public ICollection<AccountHistory> AccountHistory { get; set; }
    }
}