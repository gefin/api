using System;
using api.Models;
using System.ComponentModel.DataAnnotations;

namespace api.Models.Finance
{
    public class AccountHistory : Model
    {
        [Required]
        public int Amount { get; set; }
        [Required]
        public DateTime Date  { get; set; }
        public string Obs { get; set; }
        [Required]
        public Account Account { get; set; }
        [Required]
        public MovementType MovementType { get; set; }
    }
}