using System;
using api.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace api.Models.Finance
{
    public class Income : Model
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Amount { get; set; }
        [Required]
        public EntryPeriod EntryPeriod { get; set; }
        public ICollection<IncomeHistory> IncomeHistory { get; set; }
    }
}