using System;
using api.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace api.Models.Finance
{
    public class IncomeGet : Model
    {
        public string Name { get; set; }
        public int Amount { get; set; }
        public EntryPeriod EntryPeriod { get; set; }
    }
}