using System;
using api.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace api.Models.Finance
{
    public class IncomeInsert
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1, 100000)]
        public int Amount { get; set; }
        [Required]
        [Range(1, 3)]
        public EntryPeriod EntryPeriod { get; set; }
    }
}