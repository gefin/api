using System;
using api.Models;

namespace api.Models.Finance
{
    public enum EntryPeriod
    {
        Biweekly = 1,
        Montly = 2,
        Yearly = 3
    }
}